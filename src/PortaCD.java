import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Objects;
public class PortaCD {
    private ArrayList<CD>dischi;

    public PortaCD() {
        dischi=new ArrayList<CD>();
    }

    public CD getDisco(int i) {
        if(i<=dischi.size()){
            return dischi.get(i);
        }
        return null;
    }

    public void addDischi(CD cd) {
        if(!dischi.contains(cd)){
            dischi.add(cd);
        }
    }

    public boolean saveFile(String f) {
        boolean r = false;
        ObjectOutputStream out = null;
        try {
            out = new ObjectOutputStream(new FileOutputStream(f));
            out.writeObject(this);
            out.close();
            r = true;
        } catch (Exception e) { }
        return r;
    }

    public void getN(int i, CD cd) {
        dischi.set(i, cd);
    }

    public int getSize() {
        return dischi.size();
    }

    public void killCD(String titolo){
        for (int i = 0; i < getSize(); i++) {
            if(dischi.get(i).getTitolo()==titolo){
                dischi.remove(i);
            }
        }
    }

    public CD cercaCDperTitolo(String titolo){
        for (int i = 0; i < dischi.size(); i++) {
            if(dischi.get(i).getTitolo()!=null&&dischi.get(i).getTitolo().equals(titolo)){
                return dischi.get(i);
            }
        }
        return null;
    }
    public String toString(){
        String s="";
        // for (int i = 0; i < dischi.size(); i++) {
        //     s+=dischi.get(i);
        // }
        // return s;
        for (CD cd : dischi) {
            s+=cd+System.lineSeparator();
        }
        return s;
    }

    public boolean confrontaCollezione(Object o) {
        if (o == this)
            return true;
        if (!(o instanceof PortaCD)) {
            return false;
        }
        PortaCD portaCD = (PortaCD) o;
        return Objects.equals(dischi, portaCD.dischi);
    }
    public void ordina(){
        dischi.sort((a, b)->{
            return a.getTitolo().compareTo(b.getTitolo());
        });
    }
}