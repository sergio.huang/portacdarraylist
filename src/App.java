public class App {
    public static void main(String[] args) throws Exception {
        CD cd=new CD();
        CD cd2=new CD("titolo", "qualcosa", 1 ,1);
        CD cd3=new CD("titolo1", "qualcosa1", 1 ,1);
        PortaCD portaCd1 = new PortaCD();
        PortaCD portaCd2 = new PortaCD();
        portaCd1.addDischi(cd);
        portaCd1.addDischi(cd2);
        portaCd1.addDischi(cd3);
        portaCd1.addDischi(cd);
        portaCd2.addDischi(cd);
        portaCd2.addDischi(cd2);
        portaCd2.addDischi(cd3);
        System.out.println(portaCd1.confrontaCollezione(portaCd2));
        portaCd1.killCD("titolo");
        System.out.println(portaCd1.confrontaCollezione(portaCd2));
        System.out.println(portaCd2.cercaCDperTitolo("titolo1"));
        System.out.println(portaCd1.getSize());
        System.out.println(portaCd1.getDisco(4));
        System.out.println(portaCd1);
        //portaCd1.save("");
    }
}
